FROM maven:3.8.3-openjdk-17
WORKDIR /web
COPY target/main-0.0.1-SNAPSHOT.jar /web/main.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "main.jar"]
